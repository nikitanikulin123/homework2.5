var http = require('http');
var url = require('url');
var querystring = require('querystring');
const port = 3000;

var arr = [];
var i = 0;

function handler(req, res) {
    let data = '';
    req.on('data', chunk => data += chunk);
    req.on('end', () => {
        data = parse(data, req.headers['content-type']);
        if (req.url === '/reg')
            arr.push({'id': i++, 'item': data.item, 'count': data.count});
        res.writeHead(200, 'OK' , {'Content-Type': 'text/plain'});
        //res.write(`${data}`);
        res.write(JSON.stringify(arr));
        res.end();
    });
}

const server = http.createServer();
server.on('error', err => console.error(err));
server.on('request', handler);
server.on('listening', () => {
    console.log('Start HTTP on port %d', port);
});

server.listen(port);

function parse(data, type) {
    switch (type) {
        case 'application/json':
            data = JSON.parse(data);
            break;
        case 'application/x-www-form-urlencoded':
            data = querystring.parse(data);
            break;
    }
    return data;
}