'use strict';

// POST запрос
const querystring = require('querystring');
let data = querystring.stringify({
    "item":"book",
    "count": 1
});

let options = {
    hostname: 'localhost',
    port: 3000,
    path: '/reg',
    method: 'POST',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        //'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(data)
    }
};

const http = require('http');
let request = http.request(options);
request.write(data);
request.end();
request.on('response', res => {
    //console.log(`Статус ответа: ${res.statusCode}`);
    res.pipe(process.stdout);
});

// // GET запрос
//const http = require('http');
//
//http.get('http://localhost:3000/add/data=555')
//    .on('error', err => console.error(err))
//.on('response', res => {
//    console.log(`Статус ответа: ${res.statusCode}`);
//res.pipe(process.stdout);
//});